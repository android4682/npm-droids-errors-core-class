# About

Basically extending the `Error` class and adding some local variables that can be called upon later.

Not that special. But useful for me.

That's why it's labeled as a **Personal** throwable error core class.