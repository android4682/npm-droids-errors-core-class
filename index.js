class DroidsError extends Error {
	/**
	 * Constructing DroidError
	 * @param {String} errMsg **The error message*
	 * @param {String} moduleName **Custom module name*
	 * @param {Error} cause **If re-throwing pass the original cause here
	 */
	constructor(errMsg, moduleName, cause) {
		super(errMsg)
		this._errMsg = errMsg
		this._moduleName = moduleName
		this._cause = cause
	}
	/**
	 * Get error message of this error
	 * @return {String} **The error message*
	 */
	get errMsg() {
		return this._errMsg
	}
	/**
	 * Get module name of this error
	 * @return {String} **Custom module name*
	 */
	get moduleName() {
		return this._moduleName
	}
	/**
	 * Get original error of this error
	 * @return {Error} **If re-throwing pass the original cause here*
	 */
	get cause() {
		return this._cause
	}
}

module.exports = {
	DroidsError
};